require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :current_player

  def initialize(player_one, player_two)
    @board = Board.new
    @current_player = player_one
    @other_player = player_two
  end

  def play_turn
    pos = @current_player.get_move
    @board.place_mark(pos, @current_player.mark)
    switch_players!
  end

  def switch_players!
    @current_player, @other_player = @other_player, @current_player
  end

end
