class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move

    if any_winning_moves?
      winning_moves_from(winning_set, comp_positions)
    else free_spaces[rand(free_spaces.length)]
    end


  end

  def free_spaces
    positions = []

    @board.grid.each_with_index do |row_val, row_indx|
      row_val.each_with_index do |el, col_indx|
        positions << [row_indx, col_indx] if el == nil
      end
    end
    positions
  end

  def winning_set
    all_winning_moves = [[[0, 0], [0, 1], [0, 2]], [[1, 0], [1, 1], [1, 2]],
                         [[2, 0], [2, 1], [2, 2]], [[0, 0], [1, 0], [2, 0]],
                         [[0, 1], [1, 1], [2, 1]], [[0, 2], [1, 2], [2, 2]],
                         [[0, 0], [1, 1], [2, 0]], [[0, 2], [1, 1], [2, 0]]]
    winning_moves = all_winning_moves.select do |set|
      set.count { |pos| comp_positions.include? pos } >= 2
    end
    [ winning_moves[0][0], winning_moves[0][1], winning_moves[0][2] ]

  end

  def winning_moves_from(winning_set, comp_positions)
    winning_set.select do |pos|
      pos unless comp_positions.include? pos
    end.first
  end

  def comp_positions
    positions = []

    @board.grid.each_with_index do |row_val, row_indx|
      row_val.each_with_index do |el, col_indx|
        positions << [row_indx, col_indx] if el == @mark && !@mark.nil?
      end
    end
    positions
  end

  def any_winning_moves?
    return false if free_spaces.length == 9
    all_winning_moves = [[[0, 0], [0, 1], [0, 2]], [[1, 0], [1, 1], [1, 2]],
                         [[2, 0], [2, 1], [2, 2]], [[0, 0], [1, 0], [2, 0]],
                         [[0, 1], [1, 1], [2, 1]], [[0, 2], [1, 2], [2, 2]],
                         [[0, 0], [1, 1], [2, 0]], [[0, 2], [1, 1], [2, 0]]]
    winning_moves = all_winning_moves.select do |set|
      set.count { |pos| comp_positions.include? pos } >= 2
    end
    return false if winning_moves.empty?
    true
  end

end
