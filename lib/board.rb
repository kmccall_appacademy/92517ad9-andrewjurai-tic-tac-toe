class Board
  attr_accessor :grid

  def initialize(grid = nil)
    @grid = grid
    if grid.nil?
      @grid = [[nil, nil, nil],
               [nil, nil, nil],
               [nil, nil, nil]]
    end
  end

  def place_mark(pos, mark) #[0,0],:X ; row,col
    row = pos.first
    col = pos.last
    @grid[row][col] = mark
  end

  def empty?(pos)
    row = pos.first
    col = pos.last
    @grid[row][col].nil?

  end

  def winner

    return @grid.row_winner if @grid.row_winner
    return @grid.column_winner if @grid.column_winner
    return @grid.diagonal_winner if @grid.diagonal_winner

    nil
  end

  def over?
    return true if winner
    return true if !@grid.flatten.include? nil #if we dont have any nils
    false
  end

end

class Array

  def row_winner
    row = 0
    3.times do
      return :X if self[row].count(:X) == 3
      return :O if self[row].count(:O) == 3
      row += 1
    end
    nil
  end

  def column_winner
    column = 0
    3.times do
      current_column = [ self[0][column], self[1][column], self[2][column] ]
      return :X if current_column.count(:X) == 3
      return :O if current_column.count(:O) == 3
      column += 1
    end
    nil
  end

  def diagonal_winner
    diagonal_one = [ self[0][0], self[1][1], self[2][2]]
    diagonal_two = [ self[0][2], self[1][1], self[2][0]]

    return :X if diagonal_one.count(:X) == 3
    return :X if diagonal_two.count(:X) == 3

    return :O if diagonal_one.count(:O) == 3
    return :O if diagonal_two.count(:O) == 3

    nil

  end

end
