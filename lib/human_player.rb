class HumanPlayer
  attr_accessor :name, :board, :mark

  def initialize(name, mark = :X)
    @name = name
    @mark = mark
  end

  def get_move
    print "where"
    format_move(gets.chomp)
  end

  def format_move(user_input)
    int1 =  user_input.chars.first.to_i
    int2 =  user_input.chars.last.to_i
    [int1, int2]
  end

  def display(board)
    print "X"
  end

end
